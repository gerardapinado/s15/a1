// #2
console.log("Hello World")

// #3 and 4
let num1 = prompt("Provide a number")
let num2 = prompt("Provide another number")
let total = num1+num2

if(total<10){
	console.log(total)
}
else if(total>=10 && total<=20){
	alert("The difference of the two numbers are: "+(num1-num2))
}
else if(total>=21 && total<=30){
	alert("The product of the two numbers are: "+(num1*num2))
}else if(total>=30){
	alert("The quotient of the two numbers are: "+(num1/num2))
}
// #5
	let name = prompt("What is your name?")
	let age = prompt("What is your age?")

	if((name === "") || (age === "")){
		alert("are you a time traveler?")
	}
	else{
		alert(`Username: ${name} Age: ${age}`)
	}
// #6
function isLegalAge(age){
	if(age>=18){
		alert("You are of Legal age.")
	}
	else{
		alert("You are not allowed here.")
	}
}
// function call
isLegalAge(age)

// #7
switch(age){
	case '18': 
		alert("You are now allowed to party.")
		break;
	case '21': 
		alert("You are now part of the adult society.")
		break;
	case '65': 
		alert("We thank you for your contribution to society.")
		break;
	default:
		alert("Are you sure you're not an alien?")
}

